# Hybrid Mining Cryptocurrency - Research

## Blockchain – Ledger

Blockchain is a distributed ledger with growing lists of records that are securely linked together through a cryptographic hash. Each block contains a cryptographic hash of the previous block, a timestamp, and the transaction data. In addition that each block contains information about the previous block, they effectively form one chain, with each additional block connected to those preceding it. Consequently, blockchain transactions are so far irreversible since, once recorded, data in any given block cannot be modified recursively without changing all subsequent blocks.

## Proof of Work (PoW)

The Proof of Work (PoW) process describes a consensus mechanism that requires a significant amount of computing power and effort from a network of devices. The concept was originally adapted and implemented using the secure hash algorithm, or SHA. Bitcoin became the first widely adopted application of PoW's idea. Cryptocurrency and Bitcoin are technically a cryptocurrency that interact with Blockchain technology. A user in possession of a bitcoin number can exchange it for something of equal value, such as a glass of lemonade.

## Proof of Stake (PoS)

The Proof of Stake (PoS) process describes a consensus mechanism for processing transactions and creating new blocks on the blockchain – Ledger. This mechanism is a method for validating entries and keeping them safe on the blockchain – Ledger. PoS reduces to a fairly large extent the amount of computational work required to verify blocks and transactions. This process changes the way blocks are verified so it doesn't have to require a lot of computing power. Users offer or not a quantity of coins as a guarantee, in order to be able to validate blocks and earn rewards.

## Hashes

With the validation process, once a block is closed, the hash must be verified before a new block is opened. The hash is a 64-digit encrypted hexadecimal number. With modern technology, a hash can be created in milliseconds for a large amount of data. However, users try to solve the hash, which takes some time in computational terms. In this way, the process of certification and security of blocks in Blockchain – Ledger is also carried out.

## PoW vs PoS

In the cryptocurrency process, both consent mechanisms help Blockchain-Ledger synchronize data, validate information, and process transactions. Each method has proven successful in maintaining a blockchain-Ledger, although each has advantages and disadvantages. However, the two algorithms have very different approaches.

### PoW

In PoW users are called Miners. Mines works to solve and authenticate each block, to verify transactions.

### PoS

In PoS, users are called Validators. Validator verifies the activity and keeps records.

## Mining Algorithms

### SHA

SHA is a secure hash algorithm. The SHA is a modified version of MD5 and is used to hash data and certificates. This algorithm called a Secure hash Algorithm. This algorithm works in such a way that the input data is split into a smaller format that cannot be understood using Bitwise functions, modular additions, and compression functions. SHA works in such a way, even if one character of the message changes, then to create different fragmentation.

### Scrypt

Scrypt is a password-based key derivation function (KDF). In cryptography, a KDF is a hash function that derives one or more secret keys from a secret value such as a master key, a password, or a passphrase using a pseudorandom function. KDFs are generally efficient at preventing brute force password guessing attacks. Prior to the development of Scrypt, however, KDFs such as Password-Based Key Derivation Function 2 (PBKDF2) were limited in their ability to resist FPGAs and ASICs. PBKDF2 and other password-based KDFs were computationally intensive but not memory intensive. Scrypt was designed to be both computationally intensive and memory intensive.

### DaggerHashimoto

DaggerHashimoto was a precursor research algorithm for Ethereum mining that Ethash superseded. It was an amalgamation of two different algorithms: Dagger and Hashimoto. It was only ever a research implementation and was superseded by Ethash by the time Ethereum Mainnet launched. 

### Equihash

The Equihash algorithm is an asymmetric memory-orientated proof-of-work system that is based on the generalized birthday problem. Equihash is memory-orientated in that it is ‘memory-hard’, meaning that the amount of proof-of-work mining that can be done is predominantly determined by how much memory i.e. RAM that one possesses. In other words, memory-hard refers to a situation in which the time taken to complete a given computational problem is primarily decided by the amount of memory required to hold data. 

### RandomX

RandomX is a proof-of-work (PoW) algorithm that is optimized for general-purpose CPUs. RandomX uses random code execution (hence the name) together with several memory-hard techniques to minimize the efficiency advantage of specialized hardware. RandomX utilizes a virtual machine that executes programs in a special instruction set that consists of integer math, floating point math and branches. These programs can be translated into the CPU's native machine code on the fly. At the end, the outputs of the executed programs are consolidated into a 256-bit result using a cryptographic hashing function (Blake2b). 

## Mining Pool

Above mentioned the way in which cryptocurrencies can be produced and mined, along with all the possible technologies they use. In cryptocurrency mining, Mining Pool is the pooling of resources by Miners, who share their processing power through a network to split the reward equally, depending on the amount of work they contributed to the chance of finding blocks. By using the Mining Pool, the share acquired by the team is awarded to its members, who present valid proof of work. The use of this model is for users and in particular Miners to pool their resources so that they can create and mine cryptocurrencies more quickly and receive a portion of the reward.

### Types of Pools

- Pay-per-Share
- Proportional
- Pooled Mining
- Pay-per-last-N-shares
- Solo Mining Pool
- Peer-to-Peer Mining Pool
- Geometric Method
- Double Geometric Method

## Mining Hardware

- CPU / GPU

	The Central Processing Unit (CPU) is the computer component that's responsible for 	interpreting and executing most of the commands from the computer's other hardware and 	software. 
	
	A Graphics Processing Unit (GPU) is a specialized electronic circuit initially designed to 	accelerate computer graphics and image processing (either on a video card or embedded on 	the motherboards, mobile phones, personal computers, workstations, and game consoles). 
	
- ASIC

	An ASIC, or application-specific integrated circuit, is a microchip designed for a special 	application, such as a kind of transmission protocol or a hand-held computer.  You might 	contrast an ASIC with general integrated circuits, such as the microprocessor or random 	access memory chips in your PC. ASICs can have different designs that allow specific 	actions to be taken inside of a particular device. The two primary design methods are gate-	array and full-custom design.

- VPS

	A virtual private server(VPS) is a machine that hosts all the software and data required to 	run an application or website. It is called virtual because it only consumes a portion of the 	server's underlying physical resources which are managed by a third-party provider. 	However, you get access to your dedicated resources on that hardware. 

- Smartphone

	A smartphone is a portable computer device that combines mobile telephone functions and 	computing functions into one unit. They are distinguished from older-design feature phones 	by their more advanced hardware capabilities and extensive mobile operating systems.

https://en.bitcoinwiki.org/wiki/Mining_hardware_list	

## Community

Crypto communities are dedicated groups of individuals with a common interest in gaining knowledge about cryptocurrencies and blockchain technology. This is with the aim of building a crypto-related initiative, becoming a thought leader in the crypto industry, or investing in cryptocurrencies.	

## Conclusion

This research developed and presented technologies, standards and problems that have brought about the community and the Ecosystem of cryptocurrencies. Each cryptocurrency has its own standards and technologies that make it unique. However, this research wants to show and present the diversity of cryptocurrencies in many areas and to develop to a small extent a centralized Ecosystem in the field of cryptocurrencies and beyond.

## App

<p align="center"><img src="Hybrid_Cloud_Solution.png" alt="Hybrid_Cloud_Solution" width="700" height="200"/></p>